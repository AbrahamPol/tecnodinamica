package com.bolsadeideas.springboot.app.controllers;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Facturacion;
import com.bolsadeideas.springboot.app.models.entity.FacturacionSin;
import com.bolsadeideas.springboot.app.models.entity.HistorialFacturacion;
import com.bolsadeideas.springboot.app.models.entity.HistorialFacturacionsin;
import com.bolsadeideas.springboot.app.models.service.FacturacionsinService;

@Controller
@Secured("ROLE_ADMIN")
@SessionAttributes(value = { "facturacion", "porcentaje" })
@RequestMapping("facturacionsin")
public class FacturacionSinController {

	// listar facturas concluidas
	@Autowired
	FacturacionsinService facservice;

	@RequestMapping("")
	public String principal(Map<String, Object> model) {
		List<FacturacionSin> historial = facservice.findAll();
		model.put("historial", historial);
		return "listar-facturacionsin";
	}

	// crear la cotizacion primera vista
	@RequestMapping(value = "/crear")
	public String crear(Map<String, Object> model) {
		// agregar titulo y objeto cotizacion
		model.put("titulo", "Crear Facturacion");
		FacturacionSin facturacion = new FacturacionSin();
		model.put("facturacion", facturacion);
		// buscamos las empresas locales y las del catalogo y tambien las mandamos a
		// form
		return "formulario-facturacionsin";
	}

	// guardar la facturacion
	@RequestMapping(value = "/crear2", method = RequestMethod.POST)
	public String guardar2(@Validated FacturacionSin facturacion, BindingResult result, Map<String, Object> model,
			SessionStatus status, RedirectAttributes flash) {
		HistorialFacturacionsin historial = new HistorialFacturacionsin();

		if (result.hasErrors()) {
			if (result.hasErrors()) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje", "El numero de proceso no se puede repetir");
			}
			if (facturacion.getProceso().equals("")) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje1", "El numero de proceso es requerido");
			}
			if (facturacion.getEstatus().equals("")) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje2", "El estatus  es requerido");
			}

			return "formulario-facturacionsin";
		}

		try {
			facturacion.setMoneda("");
			facturacion.setPedido("");
			facservice.save(facturacion);
		} catch (Exception e) {
			model.put("titulo", "Crear Facturacion");
			model.put("unico", "El numero de proceso  no se puede repetir");
			return "formulario-facturacionsin";
		}
		historial.setFacturacion(facturacion);
		historial.setNo_pago("");
		historial.setPorcentaje("");
		historial.setAbonado(0);
		historial.setArchivo("");
		facservice.saveHistorial(historial);
		status.setComplete();
		flash.addFlashAttribute("success", "Facturacion guardada con Exito!");
		return "redirect:/facturacionsin";

	}
////////////////EDITAR///////////////////////77
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/editar/{id}")
	public String cargarFacturaciones(@PathVariable(name = "id") Long id, Map<String, Object> model) {

		FacturacionSin facturacion = facservice.findById(id);
		float abonado = 0;
		float restante = 0;
		for (HistorialFacturacionsin historialFacturacion : facturacion.getHistorial()) {
			abonado += historialFacturacion.getAbonado();
		}
		restante = facturacion.getTotal() - abonado;
		model.put("facturacion", facturacion);
		model.put("abonado", abonado);
		model.put("restante", restante);
		return "vista-facturacionsin";
	}
	
	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public String actualizar(@Validated HistorialFacturacionsin historialfacturacion, BindingResult result,
			RedirectAttributes flash, SessionStatus status, Map<String, Object> model) {
//			System.out.println(historialFacturacion);
		if (result.hasErrors()) {
			model.put("titulo", "Nueva Facturacion");
			return "vista-facturacion";
		}

		facservice.saveHistorial(historialfacturacion);
		flash.addFlashAttribute("success", "Facturacion actualizada con Exito!");
		status.setComplete();
		return "redirect:/facturacionsin";
	}


	//////////////77
	
	/*
	 * Crear metodo handler para registrar un nuevo pago
	 */

	// guardar la facturacion
	@RequestMapping(value = "/pagoss", method = RequestMethod.POST)
	public String nuevoPago(@Valid FacturacionSin facturacion2, BindingResult result, Map<String, Object> model,
			SessionStatus status, RedirectAttributes flash, @RequestParam(name = "file") MultipartFile foto,
			@RequestParam(name = "porcentaje") String porcentaje) {
		FacturacionSin facturacion= facservice.findById(facturacion2.getId());
		HistorialFacturacionsin historial = new HistorialFacturacionsin();
		if (!foto.isEmpty()) {
			try {// codigo para guardar la imagen en una ruta externa, que es lo recomendable
				String rootPath = "C://Temp/uploads";
				byte[] bytes = foto.getBytes();
				Path rutacompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
				Files.write(rutacompleta, bytes);
				flash.addFlashAttribute("info", "Has subido correctamente '" + foto.getOriginalFilename() + "'");
				historial.setArchivo(foto.getOriginalFilename());
			} catch (Exception e) {
				System.out.println("Error file" + e);
			}
		}

		if (result.hasErrors() || foto.isEmpty() || porcentaje.equals("") || porcentaje.equals("Elige una opcion")) {
		
			if (result.hasErrors()) {
				model.put("titulo", "Nuevo Pago");
				model.put("mensaje", "Error");
			}

			if (foto.isEmpty()) {
				model.put("titulo", "Nuevo Pago");
				model.put("mensaje2", "No puede estar vacio");
			}

			if (porcentaje.equals("")) {
				model.put("titulo", "Nuevo pago");
				model.put("mensaje3", "El porcentaje es requerido");
			}

			if (porcentaje.equals("Elige una opcion")) {
				model.put("titulo", "Nuevo pago");
				model.put("mensaje3", "El porcentaje es requerido");
			}

			return "formulario-facturacionsin-pago";
		}

		// mostrar el porcentaje de acuerdo a su deuda
		float total1 = facturacion.getTotal();
		float porcentaje1 = Float.parseFloat(porcentaje);
		float abonado = (total1 * porcentaje1) / 100;

		// buscar el numero de pago
		int numero = 1;
		int porcentajefor = 0;
		for (HistorialFacturacionsin historialFacturacion : facturacion.getHistorial()) {
			numero++;
			porcentajefor += Integer.parseInt(historialFacturacion.getPorcentaje());
		}

		int pocentajetotal = Integer.parseInt(porcentaje) + porcentajefor;
		if (pocentajetotal == 100) {
			facturacion.setEstatus("PAGADO");
		}

		historial.setNo_pago(String.valueOf(numero));
		historial.setPorcentaje(porcentaje);
		historial.setAbonado(abonado);
		historial.setFacturacion(facturacion);
		System.out.println(historial);
		facservice.save(facturacion);
		facservice.saveHistorial(historial);
		status.setComplete();
		flash.addFlashAttribute("success", "Pago guardado con Exito!");
		return "redirect:/facturacionsin/editar/" + facturacion.getId();

	}

	// mostrar la vista nueva para crear el pago
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pago/{id}")
	public String cargarFacturacionPago(@PathVariable(name = "id") Long id, Map<String, Object> model) {
		FacturacionSin facturacion = facservice.findById(id);
		float porcentaje = 0;
		for (HistorialFacturacionsin historialFacturacion : facturacion.getHistorial()) {
			porcentaje += Float.parseFloat(historialFacturacion.getPorcentaje());
		}
		model.put("facturacion", facturacion);
		model.put("porcentaje", porcentaje);
		return "formulario-facturacionsin-pago";
	}

	
	///////////////////////REGISTRAR PEDIDO/////////////////////////////////
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pedido/{id}")
	public String VistaPedido(@PathVariable(name = "id") Long id, Map<String, Object> model) {
		FacturacionSin facturacion = facservice.findById(id);
		model.put("facturacion", facturacion);
		return "formulario-pedido";
	}
	
	@RequestMapping(value = "/pedido", method = RequestMethod.POST)
	public String guardar(@Validated FacturacionSin facturacion, BindingResult result, Map<String, Object> model,
			SessionStatus status, RedirectAttributes flash, @RequestParam(name = "file") MultipartFile foto,
			@RequestParam(name = "porcentaje") String porcentaje) {
		
		FacturacionSin f=facservice.findByPedido(facturacion.getPedido());
		String ped="";
		if (f!=null) {
			ped=f.getPedido();
		}
		HistorialFacturacionsin historial = new HistorialFacturacionsin();
		if (!foto.isEmpty()) {
			try {// codigo para guardar la imagen en una ruta externa, que es lo recomendable
				String rootPath = "C://Temp/uploads";
				byte[] bytes = foto.getBytes();
				Path rutacompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
				Files.write(rutacompleta, bytes);
				flash.addFlashAttribute("info", "Has subido correctamente '" + foto.getOriginalFilename() + "'");
				historial.setArchivo(foto.getOriginalFilename());
			} catch (Exception e) {
				System.out.println("Error file" + e);
			}
		}

		if (result.hasErrors() || 
				foto.isEmpty() || 
				porcentaje.equals("") ||
				porcentaje.equals("Elige una opcion") ||
				facturacion.getPedido().equals("")||
				facturacion.getMoneda().equals("")||
				facturacion.getSubtotal()<=0||
				facturacion.getTotal()<=0||
				ped.equals(facturacion.getPedido())
				) {
			if (result.hasErrors()) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje", "No puede estar vacio");
			}//ERRORS

			if (	ped.equals(facturacion.getPedido())) {
				model.put("titulo", "Crear Facturacion");
				model.put("unico", "No se puede repetir el numero de pedido");
			}//PEDIDO
			
			
			if (foto.isEmpty()) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje2", "No puede estar vacio");
			}//FOTO

			if (porcentaje.equals("")) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje3", "El porcentaje es requerido");
			}//PROCENTAJE

			if (porcentaje.equals("Elige una opcion")) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje3", "El porcentaje es requerido");
			}//PORCENTAJE
			
			if (facturacion.getPedido().equals("")) {
				model.put("titulo", "Crear Facturacion");
				model.put("ped", "El pedido es requerido");
			}//PEDIDO
			
			if (facturacion.getMoneda().equals("")) {
				model.put("titulo", "Crear Facturacion");
				model.put("mon", "El tipo de moneda es requerido");
			}//MONEDA
			
			if (facturacion.getSubtotal()<=0) {
				model.put("titulo", "Crear Facturacion");
				model.put("sub", "El subtotal  debe ser mayor a $0");
			}//SUBTOTAL
			
			if (facturacion.getTotal()<=0) {
				model.put("titulo", "Crear Facturacion");
				model.put("tot", "El total  debe ser mayor a $0");
			}//TOTAL

			return "formulario-pedido";
		}

		if (porcentaje.equals("100")) {
			facturacion.setEstatus("PAGADO");
		} else {
			facturacion.setEstatus("NO PAGADO");
		}

		try {
			facservice.save(facturacion);
		} catch (Exception e) {
			model.put("titulo", "Crear Facturacion");
			model.put("unico", "El numero de proceso o pedido no se pueden repetir");
			return "formulario-pedido";
		}
		float total1 = facturacion.getTotal();
		float porcentaje1 = Float.parseFloat(porcentaje);
		float abonado = (total1 * porcentaje1) / 100;
		historial.setFacturacion(facturacion);
		historial.setNo_pago("1");
		historial.setPorcentaje(porcentaje);
		historial.setAbonado(abonado);
		facservice.saveHistorial(historial);
		status.setComplete();
		flash.addFlashAttribute("success", "Facturacion guardada con Exito!");
		return "redirect:/facturacionsin";

	}

	@RequestMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(name = "id") Long id) {
		facservice.delete(id);
		return "redirect:/facturacionsin";
	}


	///////////////////////////////////////////////// AUTOCOMPLETES //
	@GetMapping(value = "/cargar-facturas/{term}", produces = { "application/json" })
	public @ResponseBody List<FacturacionSin> cargarLocal(@PathVariable String term) {
		System.out.println("Estoy recibiendo "+term);
		System.out.println(facservice.findByProceso2(term));
		return facservice.findByProceso2(term);
	}

}
