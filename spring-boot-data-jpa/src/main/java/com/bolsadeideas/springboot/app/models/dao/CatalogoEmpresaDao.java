package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;
import com.bolsadeideas.springboot.app.models.entity.EmpresaLocal;

public interface CatalogoEmpresaDao extends CrudRepository<CatalogoEmpresa, Long>{

	@Query("select e from CatalogoEmpresa e where e.nombre like %?1% and e.status=1")
	public List<CatalogoEmpresa> findByNombre(String term);
	@Query("select e from CatalogoEmpresa e where e.nombre=?1 and e.status=1")
	public CatalogoEmpresa solonombre(String nombre);
	@Query(value="select * from catalogo_empresa e where status=1",nativeQuery=true)
	public List<CatalogoEmpresa> findAllTrue();
}
