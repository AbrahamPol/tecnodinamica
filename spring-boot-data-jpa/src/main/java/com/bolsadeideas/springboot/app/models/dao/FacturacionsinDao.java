package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.bolsadeideas.springboot.app.models.entity.FacturacionSin;

public interface FacturacionsinDao extends CrudRepository<FacturacionSin, Long> {

	@Query("select f from FacturacionSin f where f.pedido like %?1%")
	public List<FacturacionSin> findByProceso(String term);
	
	@Query(value="select * from FacturacionSin  where proceso like %?1%",nativeQuery=true)
	public List<FacturacionSin> findByProceso2(String term);
}
