package com.bolsadeideas.springboot.app.controllers;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import com.bolsadeideas.springboot.app.models.service.ConcluirService;
import com.bolsadeideas.springboot.app.models.service.ProductoService;

@Controller
@Secured("ROLE_ADMIN")

@RequestMapping("producto")
@SessionAttributes("producto")
public class ProductoController {

	@Autowired
	ProductoService productoservice;

	@Autowired
	ConcluirService concluirservice;
	// listar productos
	@RequestMapping("")
	public String principal(Map<String, Object> model) {
		List<Producto> producto = productoservice.todos();
		model.put("producto", producto);
		return "listar-productos";
	}

	// crear formulario con datos
	@RequestMapping(value = "/editar/{id}")
	public String formularioIdl(Map<String, Object> model, @PathVariable(value = "id") Long id) {
		Producto producto = productoservice.finById(id);
		model.put("producto", producto);
		return "vista-producto";
	}

	// funcion de actualizar el producto
	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public String actualizar(@Validated Producto producto, BindingResult result, RedirectAttributes flash,
			SessionStatus status, Map<String, Object> model) {
//		System.out.println(producto);
		if (result.hasErrors()) {
			model.put("titulo", "Nuevo Producto");

			model.put("error2", "El precio es requerido");

			return "vista-producto";
		}

		productoservice.guardarActualizar(producto);
		flash.addFlashAttribute("success", "Producto actualizado con Exito!");
		status.setComplete();
		return "redirect:/producto";
	}

	// _---------------------------------------------------------------------------
	// crear la vista guardar
	@RequestMapping(value = "/crear")
	public String crear(Map<String, Object> model) {
		// agregar titulo y objeto producto
		Producto producto = new Producto();
		model.put("producto", producto);
		model.put("titulo", "Nuevo Producto");
		// buscamos las empresas locales y las del catalogo y tambien las mandamos a
		// form
		return "formulario-producto";
	}

	// guardar el producto
	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public String guardar(@Validated Producto producto, BindingResult result, Map<String, Object> model,
			SessionStatus status, RedirectAttributes flash) {
		if (result.hasErrors()) {
			model.put("titulo", "Nuevo Producto");
			model.put("error2", "El precio es requerido");

			return "formulario-producto";
		}

		productoservice.guardarActualizar(producto);
		status.setComplete();
		flash.addFlashAttribute("success", "Producto guardado con Exito!");
		return "redirect:/producto";

	}

	// eliminar productos
	@RequestMapping(value = "/eliminar/{id}")
	public String eliminarl(Map<String, Object> model, @PathVariable(value = "id") Long id) {
		productoservice.deleteByID(id);
		return "redirect:/producto";
	}
	
	/////////////////////////////////////////////////AUTOCOMPLETES	//
	@GetMapping(value = "/cargar-productos/{term}", produces = { "application/json" })
	public @ResponseBody List<Producto> cargarProductos(@PathVariable String term, Map<String, Object> model) {
		List<Producto> producto=  concluirservice.findByNombre(term);
		model.put("producto",producto);
		return concluirservice.findByNombre(term);
	}
	
}
