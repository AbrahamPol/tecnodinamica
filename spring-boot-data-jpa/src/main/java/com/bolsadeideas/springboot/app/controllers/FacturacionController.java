package com.bolsadeideas.springboot.app.controllers;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.dao.FacturacionDao;
import com.bolsadeideas.springboot.app.models.entity.EmpresaLocal;
import com.bolsadeideas.springboot.app.models.entity.Facturacion;
import com.bolsadeideas.springboot.app.models.entity.HistorialFacturacion;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import com.bolsadeideas.springboot.app.models.service.FacturacionService;

@Controller
@Secured("ROLE_ADMIN")

//@SessionAttributes("facturacion")
@SessionAttributes(value = { "facturacion", "porcentaje" })
@RequestMapping("facturacion")
public class FacturacionController {
	// listar facturas concluidas
	@Autowired
	FacturacionService facservice;

	@RequestMapping("")
	public String principal(Map<String, Object> model) {
		List<Facturacion> historial = facservice.findAll();
		model.put("historial", historial);
		return "listar-facturacion";
	}

	// crear la cotizacion primera vista
	@RequestMapping(value = "/crear")
	public String crear(Map<String, Object> model) {
		// agregar titulo y objeto cotizacion
		model.put("titulo", "Crear Facturacion");
		Facturacion facturacion = new Facturacion();
		model.put("facturacion", facturacion);
		// buscamos las empresas locales y las del catalogo y tambien las mandamos a
		// form
		return "formulario-facturacion";
	}

	@RequestMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(name = "id") Long id) {
		facservice.delete(id);
		return "redirect:/facturacion";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/editar/{id}")
	public String cargarFacturaciones(@PathVariable(name = "id") Long id, Map<String, Object> model) {

		Facturacion facturacion = facservice.findById(id);
		float abonado = 0;
		float restante = 0;
		for (HistorialFacturacion historialFacturacion : facturacion.getHistorial()) {
			abonado += historialFacturacion.getAbonado();
		}
		restante = facturacion.getTotal() - abonado;
		model.put("facturacion", facturacion);
		model.put("abonado", abonado);
		model.put("restante", restante);
		return "vista-facturacion";
	}
	// acciones actualizar y guardar

	// funcion de actualizar el producto
	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public String actualizar(@Validated HistorialFacturacion historialfacturacion, BindingResult result,
			RedirectAttributes flash, SessionStatus status, Map<String, Object> model) {
//			System.out.println(historialFacturacion);
		if (result.hasErrors()) {
			model.put("titulo", "Nueva Facturacion");
			return "vista-facturacion";
		}

		facservice.saveHistorial(historialfacturacion);
		flash.addFlashAttribute("success", "Facturacion actualizada con Exito!");
		status.setComplete();
		return "redirect:/facturacion";
	}

	// guardar la facturacion
	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public String guardar(@Validated Facturacion facturacion, BindingResult result, Map<String, Object> model,
			SessionStatus status, RedirectAttributes flash, @RequestParam(name = "file") MultipartFile foto,
			@RequestParam(name = "porcentaje") String porcentaje) {
		HistorialFacturacion historial = new HistorialFacturacion();
		if (!foto.isEmpty()) {
			try {// codigo para guardar la imagen en una ruta externa, que es lo recomendable
				String rootPath = "C://Temp/uploads";
				byte[] bytes = foto.getBytes();
				Path rutacompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
				Files.write(rutacompleta, bytes);
				flash.addFlashAttribute("info", "Has subido correctamente '" + foto.getOriginalFilename() + "'");
				historial.setArchivo(foto.getOriginalFilename());
			} catch (Exception e) {
				System.out.println("Error file" + e);
			}
		}

		if (result.hasErrors() || foto.isEmpty() || porcentaje.equals("") || porcentaje.equals("Elige una opcion")) {
			if (result.hasErrors()) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje", "No puede estar vacio");
			}

			if (foto.isEmpty()) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje2", "No puede estar vacio");
			}

			if (porcentaje.equals("")) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje3", "El porcentaje es requerido");
			}

			if (porcentaje.equals("Elige una opcion")) {
				model.put("titulo", "Crear Facturacion");
				model.put("mensaje3", "El porcentaje es requerido");
			}

			return "formulario-facturacion";
		}

		if (porcentaje.equals("100")) {
			facturacion.setEstatus("PAGADO");
		} else {
			facturacion.setEstatus("NO PAGADO");
		}

		try {
			facservice.save(facturacion);
		} catch (Exception e) {
			model.put("titulo", "Crear Facturacion");
			model.put("unico", "El numero de proceso o pedido no se pueden repetir");
			return "formulario-facturacion";
		}
		float total1 = facturacion.getTotal();
		float porcentaje1 = Float.parseFloat(porcentaje);
		float abonado = (total1 * porcentaje1) / 100;
		historial.setFacturacion(facturacion);
		historial.setNo_pago("1");
		historial.setPorcentaje(porcentaje);
		historial.setAbonado(abonado);
		facservice.saveHistorial(historial);
		status.setComplete();
		flash.addFlashAttribute("success", "Facturacion guardada con Exito!");
		return "redirect:/facturacion";

	}

	/*
	 * Crear metodo handler para registrar un nuevo pago
	 */

	// guardar la facturacion
	@RequestMapping(value = "/pago", method = RequestMethod.POST)
	public String nuevoPago(@Validated Facturacion facturacion, BindingResult result, Map<String, Object> model,
			SessionStatus status, RedirectAttributes flash, @RequestParam(name = "file") MultipartFile foto,
			@RequestParam(name = "porcentaje") String porcentaje) {
		HistorialFacturacion historial = new HistorialFacturacion();
		if (!foto.isEmpty()) {
			try {// codigo para guardar la imagen en una ruta externa, que es lo recomendable
				String rootPath = "C://Temp/uploads";
				byte[] bytes = foto.getBytes();
				Path rutacompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
				Files.write(rutacompleta, bytes);
				flash.addFlashAttribute("info", "Has subido correctamente '" + foto.getOriginalFilename() + "'");
				historial.setArchivo(foto.getOriginalFilename());
			} catch (Exception e) {
				System.out.println("Error file" + e);
			}
		}

		if (result.hasErrors() || foto.isEmpty() || porcentaje.equals("") || porcentaje.equals("Elige una opcion")) {
			if (result.hasErrors()) {
				model.put("titulo", "Nuevo Pago");
				model.put("mensaje", "No puede estar vacio");
			}

			if (foto.isEmpty()) {
				model.put("titulo", "Nuevo Pago");
				model.put("mensaje2", "No puede estar vacio");
			}

			if (porcentaje.equals("")) {
				model.put("titulo", "Nuevo pago");
				model.put("mensaje3", "El porcentaje es requerido");
			}

			if (porcentaje.equals("Elige una opcion")) {
				model.put("titulo", "Nuevo pago");
				model.put("mensaje3", "El porcentaje es requerido");
			}

			return "formulario-facturacion-pago";
		}

		// mostrar el porcentaje de acuerdo a su deuda
		float total1 = facturacion.getTotal();
		float porcentaje1 = Float.parseFloat(porcentaje);
		float abonado = (total1 * porcentaje1) / 100;

		// buscar el numero de pago
		int numero = 1;
		int porcentajefor = 0;
		for (HistorialFacturacion historialFacturacion : facturacion.getHistorial()) {
			numero++;
			porcentajefor += Integer.parseInt(historialFacturacion.getPorcentaje());
		}

		int pocentajetotal = Integer.parseInt(porcentaje) + porcentajefor;
		if (pocentajetotal == 100) {
			facturacion.setEstatus("PAGADO");
		}

		historial.setNo_pago(String.valueOf(numero));
		historial.setPorcentaje(porcentaje);
		historial.setAbonado(abonado);
		historial.setFacturacion(facturacion);
		facservice.save(facturacion);
		facservice.saveHistorial(historial);
		status.setComplete();
		flash.addFlashAttribute("success", "Pago guardado con Exito!");
		return "redirect:/facturacion/editar/" + facturacion.getId();

	}

	// mostrar la vista nueva para crear el pago
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pago/{id}")
	public String cargarFacturacionPago(@PathVariable(name = "id") Long id, Map<String, Object> model) {
		Facturacion facturacion = facservice.findById(id);
		float porcentaje = 0;
		for (HistorialFacturacion historialFacturacion : facturacion.getHistorial()) {
			porcentaje += Float.parseFloat(historialFacturacion.getPorcentaje());
		}
		model.put("facturacion", facturacion);
		model.put("porcentaje", porcentaje);
		return "formulario-facturacion-pago";
	}

	///////////////////////////////////////////////// AUTOCOMPLETES //
	@GetMapping(value = "/cargar-facturas/{term}", produces = { "application/json" })
	public @ResponseBody List<Facturacion> cargarLocal(@PathVariable String term) {
		return facservice.findByPedido(term);
	}

}
