package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.SystemEnvironmentPropertySource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;
import com.bolsadeideas.springboot.app.models.service.CatalogoService;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@Secured("ROLE_ADMIN")
@RequestMapping("catalogo")
@SessionAttributes("catalogoempresa")
public class CatalogoController {

	@Autowired
	CatalogoService catalogoservice;

	// listar contizaciones no concluidas
	@RequestMapping("")
	public String principal(Map<String, Object> model) {
		List<CatalogoEmpresa> empresa = catalogoservice.todosCatalogo();
		model.put("empresa", empresa);
		return "listar-empresa-catalogo";
	}

	// pintar formulario recibiendo id
	@RequestMapping(value = "/vista/{id}")
	public String formularioIdl(Map<String, Object> model, @PathVariable(value = "id") Long id) {
		CatalogoEmpresa catalogoempresa = catalogoservice.findById(id);
		model.put("catalogoempresa", catalogoempresa);
		return "vista-empresa-catalogo";
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public String actualizar(@Validated CatalogoEmpresa catalogoempresa,BindingResult result, RedirectAttributes flash, SessionStatus status,
			@RequestParam(name = "file") MultipartFile foto	) {
		
		if (result.hasErrors()) {
			return "vista-empresa-catalogo";
		}
		
		if (!foto.isEmpty()) {
			try {// codigo para guardar la imagen en una ruta externa, que es lo recomendable
				String rootPath = "C://Temp/uploads";
				byte[] bytes = foto.getBytes();
				Path rutacompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
				Files.write(rutacompleta, bytes);
				flash.addFlashAttribute("info", "Has subido correctamente '" + foto.getOriginalFilename() + "'");
				catalogoempresa.setFoto(foto.getOriginalFilename());
			} catch (Exception e) {
				System.out.println("Error file" + e);
			}
		}
		catalogoempresa.setStatus(true);
		
		catalogoservice.guardarempresa(catalogoempresa);
		flash.addFlashAttribute("success", "Empresa actualizada con Exito!");
		status.setComplete();
		return "redirect:/catalogo";
	}

	// crear la cotizacion primera vista
	@RequestMapping(value = "/crear")
	public String crear(Map<String, Object> model) {
		// agregar titulo y objeto cotizacion
		CatalogoEmpresa catalogoempresa = new CatalogoEmpresa();
		model.put("catalogoempresa", catalogoempresa);
		model.put("titulo", "Nueva empresa");
		// buscamos las empresas locales y las del catalogo y tambien las mandamos a
		// form
		return "formulario-empresa-catalogo";
	}

	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public String guardar(@Validated CatalogoEmpresa catalogoempresa, BindingResult result, Map<String, Object> model,
			SessionStatus status, RedirectAttributes flash, @RequestParam(name = "file") MultipartFile foto) {
		if (!foto.isEmpty()) {
			try {// codigo para guardar la imagen en una ruta externa, que es lo recomendable
				String rootPath = "C://Temp/uploads";
				byte[] bytes = foto.getBytes();
				Path rutacompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
				Files.write(rutacompleta, bytes);
				flash.addFlashAttribute("info", "Has subido correctamente '" + foto.getOriginalFilename() + "'");
				catalogoempresa.setFoto(foto.getOriginalFilename());
			} catch (Exception e) {
				System.out.println("Error file" + e);
			}
		}

		if (result.hasErrors() || foto.isEmpty()) {
			if (result.hasErrors()) {
				model.put("titulo", "Nueva empresa");
				model.put("mensaje", "No puede estar vacio");
			}

			if (foto.isEmpty()) {
				model.put("titulo", "Nueva empresa");
				model.put("mensaje2", "No puede estar vacio");
			}
			return "formulario-empresa-catalogo";
		}

		catalogoempresa.setStatus(true);
		catalogoservice.guardarempresa(catalogoempresa);
		status.setComplete();
		flash.addFlashAttribute("success", "Empresa guardada con Exito!");
		return "redirect:/catalogo";

	}

	@RequestMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(name = "id") Long id) {
		CatalogoEmpresa catalogo = catalogoservice.findById(id);
		catalogo.setStatus(false);
		catalogoservice.guardarempresa(catalogo);
		return "redirect:/catalogo";
	}

}
