package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.bolsadeideas.springboot.app.models.dao.CotizacionDao;
import com.bolsadeideas.springboot.app.models.dao.FacturaDao;
import com.bolsadeideas.springboot.app.models.dao.IProductoDao;
import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.Producto;

@Service
public class ConcluirServiceImpl implements ConcluirService {

	@Autowired
	CotizacionDao cotizaciondao;
	
	@Autowired
   IProductoDao productodao;
	
	@Autowired
	FacturaDao facturadao;
	
	@Override
	public List<Cotizacion> todos() {
		return cotizaciondao.noconcluidas();
	}

	@Override
	public Cotizacion findOne(Long id) {
		 	return cotizaciondao.findOne(id);
	}

	@Override
	public List<Producto> findByNombre(String term) {
		return productodao.findByNombre(term);
	}

	@Override
	public Producto findByID(Long id) {
		return productodao.findOne(id);
	}

	@Override
	public void saveFactura(Factura factura) {
		facturadao.save(factura);
	}





}
