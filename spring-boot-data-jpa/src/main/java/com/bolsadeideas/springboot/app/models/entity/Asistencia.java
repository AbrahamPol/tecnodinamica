package com.bolsadeideas.springboot.app.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ManyToAny;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="asistencia")
public class Asistencia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty
	private String hora_entrada;
	@Column(name = "fecha")
	@Temporal(TemporalType.DATE)
	private Date fecha;
	@NotEmpty
	private String estatus;
	private String hora_salida;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="empleado_id")
	@NotNull
	@JsonIgnore
	private Empleado empleado;


	@PrePersist
	public void prepersist() {
	 fecha = new Date();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHora_entrada() {
		return hora_entrada;
	}

	public void setHora_entrada(String hora_entrada) {
		this.hora_entrada = hora_entrada;
	}





	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}



	@Override
	public String toString() {
		return "Asistencia [id=" + id + ", hora_entrada=" + hora_entrada + ", fecha=" + fecha + ", estatus=" + estatus
				+ ", hora_salida=" + hora_salida + ", empleado=" + empleado + "]";
	}


	public String getHora_salida() {
		return hora_salida;
	}


	public void setHora_salida(String hora_salida) {
		this.hora_salida = hora_salida;
	}

	

	
	
	
	
}
