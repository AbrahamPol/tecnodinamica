package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IProductoDao;
import com.bolsadeideas.springboot.app.models.entity.Producto;

@Service
public class ProductoServiceImpl implements ProductoService {

	@Autowired
	IProductoDao productodao;

	@Override
	public List<Producto> todos() {
		return (List<Producto>) productodao.findAll();
	}

	@Override
	public void guardarActualizar(Producto producto) {
		productodao.save(producto);
	}

	@Override
	public Producto finById(Long id) {
		return productodao.findOne(id);
	}

	@Override
	public void deleteByID(Long id) {
		productodao.delete(id);
	}
	

}
