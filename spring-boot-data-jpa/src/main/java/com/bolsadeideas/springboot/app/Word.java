package com.bolsadeideas.springboot.app;

import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.Facturacion;

public class Word {
	private XWPFDocument document= new XWPFDocument();
	public void crear(Cotizacion cotizacion) {
		try {
	
			//create a object of type FileOutputStream with name of the Numero_cotizacion on Cotizacion
			FileOutputStream output=new FileOutputStream(cotizacion.getNumero_cotizacion());
			//create a new object for  the document
			XWPFParagraph titulo=document.createParagraph();
			XWPFRun runTitulo=titulo.createRun();
			//Create  a single Title for QA, with the object titulo
			titulo.setAlignment(ParagraphAlignment.CENTER);
			runTitulo.setBold(true);
			runTitulo.setFontSize(15);
			runTitulo.setUnderline(UnderlinePatterns.WORDS);
			runTitulo.setText("Este es el titulo");
			runTitulo.setColor("2f66f2");
			
			//close de transaction
			document.write(output);
			output.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
