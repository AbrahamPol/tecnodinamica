package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "factura")
public class Factura implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne(cascade = { CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH, CascadeType.REMOVE })
	@JoinColumn(name = "cotizacion_id")
	private Cotizacion cotizacion;

	private boolean status;

	private String archivo;
	
	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@NotEmpty
	private String lugar;
	@NotEmpty
	private String condiciones;
	@NotEmpty
	private String programa;
	@NotEmpty
	private String certificado;
	@NotEmpty
	private String vigencia;
	@NotEmpty
	private String estandares;
	@NotEmpty
	private String terminos;
	@Column(name = "fecha")
	@Temporal(TemporalType.DATE)
	private Date fecha;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnore
	private List<ItemFactura> items;

	///
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ItemFactura> getItems() {
		return items;
	}

	public void setItems(List<ItemFactura> items) {
		this.items = items;
	}

	public void addItemFactura(ItemFactura itemfactura) {
		items.add(itemfactura);
	}

	public Cotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(Cotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getCondiciones() {
		return condiciones;
	}

	public void setCondiciones(String condiciones) {
		this.condiciones = condiciones;
	}

	public String getPrograma() {
		return programa;
	}

	public void setPrograma(String programa) {
		this.programa = programa;
	}

	public String getCertificado() {
		return certificado;
	}

	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getEstandares() {
		return estandares;
	}

	public void setEstandares(String estandares) {
		this.estandares = estandares;
	}

	public String getTerminos() {
		return terminos;
	}

	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Factura [cotizacion=" + cotizacion + ", lugar=" + lugar + ", condiciones=" + condiciones + ", programa="
				+ programa + ", certificado=" + certificado + ", vigencia=" + vigencia + ", estandares=" + estandares
				+ ", terminos=" + terminos + ", fecha=" + fecha + ", items=" + items + "]";
	}

	@PrePersist
	public void prepersist() {
		fecha = new Date();
	}

	public Factura() {
		items = new ArrayList<ItemFactura>();
	}

	public Float getTotal() {
		System.out.println("Estoy sumando");
		float total = (float) 0.0;
		for (ItemFactura itemFactura : items) {
			total += itemFactura.CalcularImporte()+(itemFactura.CalcularImporte())*.16;
		}
		return total;
	}

	public String getLetra() {
		String letra="";
		String letra2="";
		float total = (float) 0.0;
		for (ItemFactura itemFactura : items) {
			total += itemFactura.CalcularImporte()+(itemFactura.CalcularImporte())*.16;
		}
		TareasProgramacion tarea= new TareasProgramacion();
		letra=String.valueOf(total);
		letra2=tarea.cantidadConLetra(letra);
		return letra2;
	}

	// atributo generado automaticamente por Serializable
	private static final long serialVersionUID = 1L;

}
