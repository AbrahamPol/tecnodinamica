package com.bolsadeideas.springboot.app.models.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Facturacionsin")
public class FacturacionSin {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;


	private String pedido;

	@Column(unique = true)
	@NotEmpty
	private String proceso;

	
	private float subtotal;

	
	private float total;

	
	private String moneda;

	private String estatus;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "facturacion_id")
	@JsonIgnore
	private List<HistorialFacturacionsin> historial;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPedido() {
		return pedido;
	}

	public void setPedido(String pedido) {
		this.pedido = pedido;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public float getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(float subtotal) {
		this.subtotal = subtotal;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public List<HistorialFacturacionsin> getHistorial() {
		return historial;
	}

	public void setHistorial(List<HistorialFacturacionsin> historial) {
		this.historial = historial;
	}

	@Override
	public String toString() {
		return "FacturacionSin [id=" + id + ", pedido=" + pedido + ", proceso=" + proceso + ", subtotal=" + subtotal
				+ ", total=" + total + ", moneda=" + moneda + ", estatus=" + estatus + "]";
	}

}
