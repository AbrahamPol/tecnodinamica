package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.EmpleadoDao;
import com.bolsadeideas.springboot.app.models.entity.Empleado;
@Service
public class EmpleadoServiceImpl implements EmpleadoService {

	@Autowired
	EmpleadoDao empleadoDao;


	@Override
	public Empleado findEmpleadoByID(Long id) {
		return empleadoDao.findOne(id);
	}

	@Override
	public void deleteEmpleado(Empleado empleado) {
		empleado.setStatus(false);
		empleadoDao.save(empleado);
	}

	@Override
	public List<Empleado> findAllEmpleados() {
		return empleadoDao.findAllTrue();
	}

	@Override
	public void saveEmpleado(Empleado empleado) {
		empleadoDao.save(empleado);
	}

	@Override
	public List<Empleado> findByNombre(String term) {
	return empleadoDao.findByNombre(term);
	}

}
