package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.FacturacionDao;
import com.bolsadeideas.springboot.app.models.dao.HistorialFacturacionDao;
import com.bolsadeideas.springboot.app.models.entity.Facturacion;
import com.bolsadeideas.springboot.app.models.entity.HistorialFacturacion;

@Service
public class FacturacionServiceImpl implements FacturacionService {
	@Autowired
	FacturacionDao facdao;

	@Autowired
	HistorialFacturacionDao historialdao;

	@Override
	public void save(Facturacion facturacion) {
		facdao.save(facturacion);
	}

	@Override
	public List<Facturacion> findAll() {
		return (List<Facturacion>) facdao.findAll();
	}

	@Override
	public Facturacion findById(Long id) {
		return facdao.findOne(id);
	}

	@Override
	public void update(Facturacion facturacion) {
		facdao.save(facturacion);
	}

	@Override
	public void delete(Long id) {
		facdao.delete(id);
	}

	@Override
	public void saveHistorial(HistorialFacturacion historial) {
		historialdao.save(historial);

	}

	@Override
	public HistorialFacturacion findOne(Long id) {
		return historialdao.findOne(id);

	}

	@Override
	public List<HistorialFacturacion> findAllHistorial() {
		return (List<HistorialFacturacion>) historialdao.findAll();
	}

	@Override
	public void updateHistorial(HistorialFacturacion historial) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actualizarPago(float abonado, String archivo, String pago, String Porcentaje, Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Facturacion> findByPedido(String term) {
		return (List<Facturacion>) facdao.findByPedido(term);
	}

//	@Override
//	public void actualizarPago(float abonado, String archivo, String pago, String Porcentaje, Long id) {
//		historialdao.actualizarPago(abonado, archivo, pago, Porcentaje, id);
//		
//	}

}
