package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.FacturacionsinDao;
import com.bolsadeideas.springboot.app.models.dao.HistorialFacturacionsinDao;
import com.bolsadeideas.springboot.app.models.entity.FacturacionSin;
import com.bolsadeideas.springboot.app.models.entity.HistorialFacturacionsin;

@Service
public class FacturacionsinServiceImpl implements FacturacionsinService {

	@Autowired
	FacturacionsinDao facdao;

	@Autowired
	HistorialFacturacionsinDao historialdao;

	@Override
	public void save(FacturacionSin facturacion) {
		facdao.save(facturacion);
	}

	@Override
	public List<FacturacionSin> findAll() {
		return (List<FacturacionSin>) facdao.findAll();
	}

	@Override
	public FacturacionSin findById(Long id) {
		return facdao.findOne(id);
	}

	@Override
	public void update(FacturacionSin facturacion) {
		facdao.save(facturacion);

	}

	@Override
	public void delete(Long id) {
		facdao.delete(id);

	}

	@Override
	public void saveHistorial(HistorialFacturacionsin historial) {
		historialdao.save(historial);

	}

	@Override
	public void updateHistorial(HistorialFacturacionsin historial) {
		// TODO Auto-generated method stub

	}

	@Override
	public HistorialFacturacionsin findOne(Long id) {
		return historialdao.findOne(id);
	}

	@Override
	public List<HistorialFacturacionsin> findAllHistorial() {
		return (List<HistorialFacturacionsin>) historialdao.findAll();
	}

	@Override
	public void actualizarPago(float abonado, String archivo, String pago, String Porcentaje, Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<FacturacionSin> findByProceso(String term) {
		return (List<FacturacionSin>) facdao.findByProceso(term);
	}

	@Override
	public FacturacionSin findByPedido(String term) {
		return null;
	}

	@Override
	public List<FacturacionSin> findByProceso2(String term) {
		return facdao.findByProceso2(term);
	}

	

	

}
