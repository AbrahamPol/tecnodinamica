package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Facturacion;
import com.bolsadeideas.springboot.app.models.entity.HistorialFacturacion;
import com.bolsadeideas.springboot.app.models.entity.Producto;

public interface FacturacionService {
//guardar
	public void save(Facturacion facturacion);
	//buscar todos
	public List<Facturacion> findAll();
	
	//buscar por id
	public Facturacion findById(Long id);
	//actualizar
	public void update(Facturacion facturacion);
	//eliminar
	public void delete(Long id);	
	
	/*
	 * Metodos para guardar el historial de pago
	 */
	public void saveHistorial(HistorialFacturacion historial);	
	public void updateHistorial(HistorialFacturacion historial);
	public HistorialFacturacion findOne (Long id);	
	public List<HistorialFacturacion> findAllHistorial();
	public void actualizarPago(float abonado,String archivo,String pago,String Porcentaje,Long id);
	public List<Facturacion> findByPedido(String term);
	
}
