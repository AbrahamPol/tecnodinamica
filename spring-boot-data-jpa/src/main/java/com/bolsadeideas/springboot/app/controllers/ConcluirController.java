package com.bolsadeideas.springboot.app.controllers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout.Alignment;
import javax.validation.Valid;

import org.apache.poi.sl.usermodel.PictureData.PictureType;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFFooter;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTabStop;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTabJc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.ItemFactura;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import com.bolsadeideas.springboot.app.models.service.ConcluirService;
import com.bolsadeideas.springboot.app.models.service.CotizacionService;
import com.bolsadeideas.springboot.app.models.service.ProductoService;

@Controller
@Secured("ROLE_ADMIN")

@RequestMapping("concluir")
@SessionAttributes("factura")
public class ConcluirController {

	@Autowired
	ConcluirService concluirservice;

	@Autowired
	private ProductoService productoService;

	// listar contizaciones no concluidas
	@RequestMapping("")
	public String principal(Map<String, Object> model) {
		List<Cotizacion> cotizacion = concluirservice.todos();
		model.put("cotizacion", cotizacion);
		return "listar-concluir";
	}

	// pintar formulario recibiendo id
	@RequestMapping(value = "/vista/{id}")
	public String formularioIdl(Map<String, Object> model, @PathVariable(value = "id") Long id) {
		Cotizacion cotizacion = concluirservice.findOne(id);
		Factura factura = new Factura();
		factura.setCotizacion(cotizacion);
		model.put("factura", factura);
		return "formulario-concluir";
	}

	@GetMapping(value = "/cargar-productos/{term}", produces = { "application/json" })
	public @ResponseBody List<Producto> cargarProductos(@PathVariable String term) {
		return concluirservice.findByNombre(term);

	}

	@PostMapping("/form")
	public String guardar(@Valid Factura factura, BindingResult result, Model model,
			@RequestParam(name = "item_id[]", required = false) Long[] itemId,
			@RequestParam(name = "cantidad[]", required = false) Integer[] cantidad, RedirectAttributes flash,
			SessionStatus status) {

		if (result.hasErrors()) {
			return "formulario-concluir";
		}

		if (itemId == null || itemId.length == 0) {
			model.addAttribute("error", "Error: La factura debe de tener al menos un producto!");
			return "formulario-concluir";
		}

		Long existenciaBD = (long) 0;
		Producto producto = null;
		for (int i = 0; i < itemId.length; i++) {
			producto = concluirservice.findByID(itemId[i]);
			existenciaBD = producto.getExistencia();
			if (existenciaBD < cantidad[i]) {
				model.addAttribute("error",
						"No puedes vender mas cantidad de lo que tienes en el inventario! " + producto.getNombre());
				return "formulario-concluir";
			}
			ItemFactura linea = new ItemFactura();
			linea.setCantidad(cantidad[i]);
			linea.setProducto(producto);
			factura.addItemFactura(linea);
		}

		/// se hace un segundo for para actualizar el inventario
		// esto para percartarnos realmente que la cantidad sea menor que la existencia
		/// en el inventario
		Long existenciaactual = (long) 0;
		for (int i = 0; i < itemId.length; i++) {
			producto = concluirservice.findByID(itemId[i]);
			existenciaBD = producto.getExistencia();
			existenciaactual = existenciaBD - cantidad[i];
			producto.setExistencia(existenciaactual);
			productoService.guardarActualizar(producto);
		}

		////////////////////////////////////////////////////////////////////////// 777
		String doc = "";
		XWPFDocument document = new XWPFDocument();
		try {
			doc = factura.getCotizacion().getNumero_cotizacion() + ".docx";
			// encabezado
			// create header start
			CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
			XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(document, sectPr);
			XWPFHeader header = headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

			//////
			XWPFParagraph paragraph = document.createParagraph();
			XWPFRun run = paragraph.createRun();
			paragraph = header.createParagraph();
			paragraph.setAlignment(ParagraphAlignment.LEFT);

			CTTabStop tabStop = paragraph.getCTP().getPPr().addNewTabs().addNewTab();
			tabStop.setVal(STTabJc.LEFT);
			int twipsPerInch = 1440;
			tabStop.setPos(BigInteger.valueOf(6 * twipsPerInch));

			run = paragraph.createRun();
			run.setItalic(true);
			run.setText("No.Cotizacion: " + factura.getCotizacion().getNumero_cotizacion() + "\n");
			run.addBreak();

			run = paragraph.createRun();
			run.setItalic(true);
			run.setText("Su referencia: " + factura.getCotizacion().getReferencia());
			run.setTextPosition(100);
			run.addBreak();

			////////////////////
			CTSectPr sectPr2 = document.getDocument().getBody().addNewSectPr();
			XWPFHeaderFooterPolicy headerFooterPolicy2 = new XWPFHeaderFooterPolicy(document, sectPr2);
			XWPFHeader header2 = headerFooterPolicy2.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

			XWPFParagraph paragraph2 = document.createParagraph();
			XWPFRun run2 = paragraph2.createRun();
			paragraph2 = header2.createParagraph();
			paragraph2.setAlignment(ParagraphAlignment.RIGHT);

			CTTabStop tabStop2 = paragraph2.getCTP().getPPr().addNewTabs().addNewTab();
			tabStop2.setVal(STTabJc.RIGHT);
			int twipsPerInch2 = 1440;
			tabStop2.setPos(BigInteger.valueOf(6 * twipsPerInch2));
			run2 = paragraph2.createRun();
			String imgFile = "C:\\Temp\\uploads\\" + factura.getCotizacion().getEmpresa_local().getFoto();
			run2.addPicture(new FileInputStream(imgFile), XWPFDocument.PICTURE_TYPE_PNG, imgFile, Units.toEMU(100),
					Units.toEMU(70));

			///////// FOTTER
			// create footer start
			XWPFFooter footer = headerFooterPolicy.createFooter(XWPFHeaderFooterPolicy.DEFAULT);
			paragraph = footer.createParagraph();
			paragraph.setAlignment(ParagraphAlignment.RIGHT);
			run = paragraph.createRun();
			run.setText("TECNO DINÁMICA DCYA SA DE CV\r\n"
					+ " Calle Laurel No. 30, Col. Villa del Real Loma Alta, Tecámac, Edo. de México, C.P. 55770\r\n"
					+ "   Tels. 55.2221.0571  www.tecnodinamica.com.mx\r\n" + "");

			// insert image the other company
			XWPFParagraph image_local2 = document.createParagraph();
			image_local2.setAlignment(ParagraphAlignment.LEFT);
			XWPFRun image_local_run2 = image_local2.createRun();
			InputStream image12 = new FileInputStream(
					"C:\\Temp\\uploads\\" + factura.getCotizacion().getEmpresa_catalogo().getFoto());
			image_local_run2.addPicture(image12, Document.PICTURE_TYPE_JPEG, "1", Units.toEMU(120), Units.toEMU(80));
			image12.close();

			// insert fecha
			XWPFParagraph fec = document.createParagraph();
			fec.setAlignment(ParagraphAlignment.RIGHT);
			XWPFRun fecrun = fec.createRun();
			fecrun.setBold(true);
			fecrun.setFontFamily("Arial");
			fecrun.setFontSize(12);
			fecrun.setText("Edo. México: " + factura.getCotizacion().getFecha() + "\n");

			XWPFParagraph direc = document.createParagraph();
			direc.setAlignment(ParagraphAlignment.LEFT);
			XWPFRun direcrun = direc.createRun();
			direcrun.setBold(true);
			direcrun.setFontFamily("Arial");
			direcrun.setFontSize(12);
			direcrun.setText(factura.getCotizacion().getEmpresa_catalogo().getDireccion());

			// nombre cargo correo of the manager align left
			XWPFParagraph nombrecargo = document.createParagraph();
			XWPFRun nombrecargorun = nombrecargo.createRun();
			nombrecargorun.setFontFamily("Arial");
			nombrecargorun.setFontSize(12);
			nombrecargorun.setBold(true);
			nombrecargorun
					.setText(factura.getCotizacion().getCargo_nombre() + " " + factura.getCotizacion().getDepartamento()
							+ " " + factura.getCotizacion().getCorreo() + " " + "\n");

			XWPFParagraph asunto = document.createParagraph();
			asunto.setAlignment(ParagraphAlignment.RIGHT);
			XWPFRun asuntorun = asunto.createRun();
			asuntorun.setBold(true);
			asuntorun.setFontFamily("Arial");
			asuntorun.setFontSize(12);
			asuntorun.setText("Asunto: " + factura.getCotizacion().getAsunto() + "\n");

			XWPFParagraph nombrecargoo = document.createParagraph();
			XWPFRun nombrecargorun2 = nombrecargoo.createRun();
			nombrecargorun2.setFontFamily("Arial");
			nombrecargorun2.setFontSize(12);
			nombrecargorun2.setText("Estimado " + factura.getCotizacion().getCargo_nombre() + ": \n");

			XWPFParagraph asunto2 = document.createParagraph();
			XWPFRun asuntorun2 = asunto2.createRun();
			asuntorun2.setFontFamily("Arial");
			asuntorun2.setFontSize(11);
			asuntorun2.setText(
					"Por este medio, tengo el agrado de presentar nuestro ajuste a la oferta Comercial para el servicio de  "
							+ factura.getCotizacion().getAsunto() + "\n");

			XWPFParagraph atentamente = document.createParagraph();
			XWPFRun atentamenterun = atentamente.createRun();
			atentamenterun.setText("ATENTAMENTE");
			atentamenterun.setFontFamily("Arial");
			atentamenterun.setFontSize(12);
			atentamenterun.addBreak();
			atentamenterun = atentamente.createRun();
			atentamenterun.setFontFamily("Arial");
			atentamenterun.setFontSize(12);
			atentamenterun.setText(factura.getCotizacion().getEmpresa_local().getNombre() + "\n");
			atentamenterun.addBreak();

			XWPFParagraph inge = document.createParagraph();
			XWPFRun ingerun = inge.createRun();
			ingerun.setBold(true);
			ingerun.setFontFamily("Arial");
			ingerun.setFontSize(12);
			ingerun.setText("Ing. Martín Márquez Moreno ");
			ingerun.addBreak();
			ingerun = inge.createRun();
			ingerun.setFontFamily("Arial");
			ingerun.setFontSize(12);
			ingerun.setBold(true);
			ingerun.setTextPosition(0);
			ingerun.setText("Gerente de Ingeniería");

			// insert here the table with the all description´s product
			XWPFTable table = document.createTable();
		
			// create first row
			XWPFTableRow tableRowOne = table.getRow(0);
			tableRowOne.getCell(0).setText("Producto");
			tableRowOne.addNewTableCell().setText("Precio   ");
			tableRowOne.addNewTableCell().setText("Cantidad     ");
			tableRowOne.addNewTableCell().setText("Subtotal       ");

			double totall = 0.0;
			double totall2 = 0.0;
			for (int i = 0; i < itemId.length; i++) {
				producto = concluirservice.findByID(itemId[i]);
				XWPFTableRow tableRowTwo = table.createRow();
				tableRowTwo.getCell(0).setText(producto.getNombre());
				tableRowTwo.getCell(1).setText("        "+String.valueOf(producto.getPrecio() + "   "));
				tableRowTwo.getCell(2).setText("      "+String.valueOf(cantidad[i]) + "   ");
				totall = cantidad[i] * producto.getPrecio();
				totall2 += cantidad[i] * producto.getPrecio();
				tableRowTwo.getCell(3).setText("     "+String.valueOf(totall) + "     ");
			}

			double totfinal = (totall2 * .16) + totall2;
			double iva = (totall2 * .16);
			XWPFTableRow tableRowThree = table.createRow();
			tableRowThree.getCell(3).setText("Subtotal  \n" + totall2 + " \nIVA: " + iva + " \nGran total: " + totfinal);

			XWPFParagraph espacio = document.createParagraph();
			XWPFRun espaciorun = espacio.createRun();
			espaciorun.setText("");
			espaciorun.addBreak();

			// after that insert the terminos
			XWPFParagraph terminos = document.createParagraph();
			terminos.setAlignment(ParagraphAlignment.LEFT);
			XWPFRun terminosrun = terminos.createRun();
			terminosrun.setBold(true);
			terminosrun.setFontFamily("Arial");
			terminosrun.setFontSize(14);
			terminosrun.setText("Términos:  " + "\n");

			XWPFParagraph lugar = document.createParagraph();
			XWPFRun lugarrun = lugar.createRun();
			lugarrun.setFontFamily("Arial");
			lugarrun.setFontSize(12);
			lugarrun.setText("Lugar y condiciones de entrega:  " + "\n" + "");
			XWPFParagraph lugar2 = document.createParagraph();
			XWPFRun lugarrun2 = lugar2.createRun();
			lugarrun2.setFontFamily("Arial");
			lugarrun2.setFontSize(10);
			lugarrun2.setText(factura.getLugar() + "\n");

			XWPFParagraph condiciones = document.createParagraph();
			XWPFRun condicionesrun = condiciones.createRun();
			condicionesrun.setFontFamily("Arial");
			condicionesrun.setFontSize(12);
			condicionesrun.setText("Condiciones de pago:  " + "\n");
			XWPFParagraph condiciones2 = document.createParagraph();
			XWPFRun condicionesrun2 = condiciones2.createRun();
			condicionesrun2.setFontFamily("Arial");
			condicionesrun2.setFontSize(10);
			condicionesrun2.setText(factura.getCondiciones() + "\n");

			XWPFParagraph programa = document.createParagraph();
			XWPFRun programarun = programa.createRun();
			programarun.setText("Programa de entrega:  " + "\n");
			programarun.setFontFamily("Arial");
			programarun.setFontSize(12);
			XWPFParagraph programa2 = document.createParagraph();
			XWPFRun programarun2 = programa2.createRun();
			programarun2.setFontFamily("Arial");
			programarun2.setFontSize(10);
			programarun2.setText(factura.getPrograma() + "\n");

			XWPFParagraph certificado = document.createParagraph();
			XWPFRun certificadorun = certificado.createRun();
			certificadorun.setFontFamily("Arial");
			certificadorun.setFontSize(12);
			certificadorun.setText("Certificado de materiales:  " + "\n");
			XWPFParagraph certificado2 = document.createParagraph();
			XWPFRun certificadorun2 = certificado2.createRun();
			certificadorun2.setFontFamily("Arial");
			certificadorun2.setFontSize(10);
			certificadorun2.setText(factura.getCertificado() + "\n");

			XWPFParagraph vigencia = document.createParagraph();
			XWPFRun vigenciarun = vigencia.createRun();
			vigenciarun.setFontFamily("Arial");
			vigenciarun.setFontSize(12);
			vigenciarun.setText("Vigencia de precios de la oferta:  " + "\n");
			XWPFParagraph vigencia2 = document.createParagraph();
			XWPFRun vigenciarun2 = vigencia2.createRun();
			vigenciarun2.setFontFamily("Arial");
			vigenciarun2.setFontSize(10);
			vigenciarun2.setText(factura.getVigencia() + "\n");

			XWPFParagraph estandares = document.createParagraph();
			XWPFRun estandaresrun = estandares.createRun();
			estandaresrun.setFontFamily("Arial");
			estandaresrun.setFontSize(12);
			estandaresrun.setText("Estándares y especificaciones:  " + "\n");
			XWPFParagraph estandares2 = document.createParagraph();
			XWPFRun estandaresrun2 = estandares2.createRun();
			estandaresrun2.setFontFamily("Arial");
			estandaresrun2.setFontSize(10);
			estandaresrun2.setText(factura.getEstandares() + "\n");

			XWPFParagraph terminos2 = document.createParagraph();
			XWPFRun terminosrun2 = terminos2.createRun();
			terminosrun2.setFontFamily("Arial");
			terminosrun2.setFontSize(12);
			terminosrun2.setText("Términos no incluidos:  " + "\n");
			XWPFParagraph terminos22 = document.createParagraph();
			XWPFRun terminosrun22 = terminos22.createRun();
			terminosrun22.setFontFamily("Arial");
			terminosrun22.setFontSize(10);
			terminosrun22.setText(factura.getTerminos() + "\n");

			XWPFParagraph banco = document.createParagraph();
			XWPFRun bancorun = banco.createRun();
			bancorun.setBold(true);
			bancorun.setFontFamily("Arial");
			bancorun.setFontSize(14);
			bancorun.setText("DATOS BANCARIOS");
			bancorun.addBreak();

			bancorun = banco.createRun();
			bancorun.setText("");
			bancorun.addBreak();

			bancorun = banco.createRun();
			bancorun.setBold(true);
			bancorun.setFontFamily("Arial");
			bancorun.setFontSize(14);
			bancorun.setText("BANCO: BANCOMER, SUC. 5243");
			bancorun.addBreak();

			bancorun = banco.createRun();
			bancorun.setText("");
			bancorun.addBreak();

			bancorun = banco.createRun();
			bancorun.setBold(true);
			bancorun.setFontFamily("Arial");
			bancorun.setFontSize(14);
			bancorun.setText("Cuenta en Dólares: 0170309516");
			bancorun.addBreak();

			bancorun = banco.createRun();
			bancorun.setFontFamily("Arial");
			bancorun.setFontSize(14);
			bancorun.setText("CLABE INTERBANCARIA USD: 012180001703095");
			bancorun.addBreak();

			bancorun = banco.createRun();
			bancorun.setBold(true);
			bancorun.setFontFamily("Arial");
			bancorun.setFontSize(14);
			bancorun.setText("Agradeceremos la notificación de su pago a:");
			bancorun.addBreak();

			bancorun = banco.createRun();
			bancorun.setText("");
			bancorun.addBreak();

			bancorun = banco.createRun();
			bancorun.setBold(true);
			bancorun.setFontFamily("Arial");
			bancorun.setFontSize(14);
			bancorun.setText("solis@tecnodinamica.com.mx");
			bancorun.addBreak();

			FileOutputStream output = new FileOutputStream(doc);
			document.write(output);
			output.close();
			Path origenPath = FileSystems.getDefault()
					.getPath("C:\\Users\\Abraham\\git\\tecno\\spring-boot-data-jpa\\" + doc);
			Path destinoPath = FileSystems.getDefault().getPath("C:\\Temp\\uploads\\" + doc);
			try {
				Files.copy(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
				Files.delete(origenPath);
			} catch (IOException e) {
				System.err.println(e);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		factura.setArchivo(doc);

		concluirservice.saveFactura(factura);
		status.setComplete();
		flash.addFlashAttribute("success", "Cotizacion creada con éxito!");
		return "redirect:/facturas";
	}

}