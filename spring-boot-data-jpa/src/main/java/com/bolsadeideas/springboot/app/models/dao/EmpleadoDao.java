package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.bolsadeideas.springboot.app.models.entity.Empleado;

public interface EmpleadoDao extends JpaRepository<Empleado, Long> {

	@Query("select e from Empleado e where e.nombre like %?1% and e.status=1")
	public List<Empleado> findByNombre(String term);
	@Query(value="select * from Empleado e where status=1",nativeQuery=true)
	public List<Empleado> findAllTrue();
}
