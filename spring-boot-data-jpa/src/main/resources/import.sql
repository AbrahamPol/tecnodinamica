

insert into empresa_local VALUES(null,"Calle Laurel No. 30, Col. Villa del Real Loma Alta, Tecámac, Edo. de México, C.P. 55770","Tecnodinamica.jpg","TECNO DINAMICA DCYA SA DE CV","http://www.tecnodinamica.com.mx","5522210571");
insert into catalogo_empresa VALUES(null,"Carr.Panamericana Celaya-Salamanca No.114  38020 Celaya, Guanajuato.","hermos.png","Hermos S.A de C.V","http://www.hermos.com.mx/",1,"(461) 618-7300");
insert into catalogo_empresa VALUES(null,"MEXICO  Av. Tochtli No. 323 - 3 Col. Fracc. Ind. San Antonio    Delegacion Azcapotzalco    Ciudad de México. C.P.02760   entre Av. Tezozomoc y calle Centeotl. ","risoul.png","Risoul","https://www.risoul.com.mx/",1,"63-89-10-70");
insert into catalogo_empresa VALUES(null,"Pescaditos No. 3, Col. Centro,Del. Cuauhtémoc, CDMX","lacasa.png","La Casa del control y El Gabinete","https://www.lacasadelcontrol.com.mx/",1,"5510 3349");
insert into catalogo_empresa VALUES(null,"Manzanillo # 807,Col. San Jerónimo Chicahualco,Metepec, Edo. de México, C.P. 52170. Toluca","DIMEINT.png","DIMEINT","https://dimeint.mx/",1,"722 275 0863");
insert into catalogo_empresa VALUES(null,"Convento de San Bernardo 5 Jardines de Santa Mónica 54050 Tlalnepantla, Estado De Mexico Mexico","mega.png","MEGA CONTROL Y SUMINISTROS","https://megaenlinea.com/",1,"(55) 6628 9413");
insert into catalogo_empresa VALUES(null,"Carretera a Eldorado km 2.5 Campo el Diez. C.P. 80300","electrotecnia.png","Electrotecnia","https://www.electrotecnicareal.com/",1,"(55) 2650-3797");
insert into catalogo_empresa VALUES(null,"Boulevard Via Atlixcayotl 5208 Torre JV1 piso 14 B | San Andrés Cholula, Puebla 72820, México","prosofttechnology.png","ProSoft Technology","https://mx.prosoft-technology.com/https://mx.prosoft-technology.com/",1,"52 222 264 18 14");
insert into catalogo_empresa VALUES(null,"COL. CENTRO DEL. CUAUHTEMOC CDMX","eci.jpeg","ECI","https://ecisacv.com/empresa/acerca-de/",1,"5521 7626");
insert into catalogo_empresa VALUES(null,"Carr.Panamericana Celaya-Salamanca No.114  38020   Celaya, Guanajuato Mexico","TICSA.png","Ticsa","http://www.ticsa.com.mx/epm.html",1,"52-553098 5600");


/* Populate tabla productos */
INSERT INTO producto (fecha,nombre, precio,existencia,marca,numero) VALUES( NOW(),'Panasonic Pantalla LCD', 9800,10,"SONY","777354345");
INSERT INTO producto (fecha,nombre, precio,existencia,marca,numero) VALUES(NOW(),'Sony Camara digital DSC-W320B', 1290,10,"SONY","777354346");
INSERT INTO producto (fecha,nombre, precio,existencia,marca,numero) VALUES(NOW(),'Apple iPod shuffle', 1499,10,"MAC","777354347");
INSERT INTO producto (fecha,nombre, precio,existencia,marca,numero) VALUES(NOW(),'Sony Notebook Z110', 3790,10,"SONY","777354348");
INSERT INTO producto (fecha,nombre, precio,existencia,marca,numero) VALUES(NOW(),'Hewlett Packard Multifuncional F2280', 6999,10,"HEWLET","777354349");
INSERT INTO producto (fecha,nombre, precio,existencia,marca,numero) VALUES(NOW(),'Bianchi Bicicleta Aro 26', 6999,10,"BIANCHI","777354350");
INSERT INTO producto (fecha,nombre, precio,existencia,marca,numero) VALUES(NOW(),'Mica Comoda 5 Cajones', 1000,10,"BB","777354351");


/* Creamos algunas facturas */
/* id,certificado,condiciones,estandares,fecha,lugar,programa,terminos,vigencia,cotizacion_id*/

/*Table inventario*/
INSERT INTO facturacion values(null,"PAGADO","MEXICANA","123","1234",100,116);
INSERT INTO historial_facturacion values(null,116,"one.pdf","1","100",1);
INSERT INTO facturacion values(null,"NO PAGADO","MEXICANA","123435","125f34",1000,1160);
INSERT INTO historial_facturacion values(null,116,"one.pdf","1","10",2);
INSERT INTO historial_facturacion values(null,116,"two.pdf","2","10",2);

INSERT INTO empleado values(null,"San vicente","Hernandez","defau.jpg","Gerson","Desarrollador trainer",1);
INSERT INTO asistencia values(null,"asistencia","2019-03-23","7:10 am","6:30 pm",1);



INSERT INTO facturacionsin values(null,"En espera","","","123456",0,0);
INSERT INTO historial_facturacionsin values(null,0,"","","",1);














